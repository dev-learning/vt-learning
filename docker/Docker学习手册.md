# 走进Docker

官网：https://www.docker.com/

官方文档：https://docs.docker.com/

github地址：https://github.com/docker/docker/

DockerHub地址：https://hub.docker.com/

## Docker是什么

​		Docker是基于Go语言编写的一个开源的应用容器引擎。

​		Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，来快速交付，测试和部署代码，您可以大大减少编写代码和在生产环境中运行代码之间的延迟。

## Docker的应用场景

- 应用的自动化部署和发布；
- 自动化测试和持续集成、持续交付（CI / CD）
- DevOps

## Docker的基本组成

Docker的架构图如下：

![Docker Architecture Diagram](images/architecture.svg)



- Docker daemon：Docker的后台进程服务，用于监听Docker的API，并管理Docker内部对象；
- Docker client：Docker客户端，提供用户和Docker的交互，用于连接Docker daemon，调用Docker的API；
- Docker registries：Docker注册表，用于存储Docker镜像，Docker Hub是一个公共的Docker注册表，提供了很多官方镜像。使用docker pull或docker run命令时，将从Docker registries下载镜像，使用docker push时，可以提交镜像到使用Docker registries。
- Docker objects
  - Images：Docker镜像，一个只读的模板，包含了创建Docker容器的说明。
  - Containers：Docker容器，容器是镜像的可运行实例。



## Docker安装

官方教程：https://docs.docker.com/get-docker/

> 注意：本教程是基于Centos7环境进行安装的

### 1、卸载旧的Docker

```shell
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 2、安装Docker

Docker有两种安装方法：

- 快捷安装（推荐）：通过执行官方提供的安装脚本，可以实现Docker的自动化安装
- 常规安装：通过一步步执行安装命令，进行安装。

#### 快捷安装（推荐）

安装命令如下：

```shell
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

也可以使用国内 daocloud 一键安装命令：

```shell
curl -sSL https://get.daocloud.io/docker | sh
```

#### 常规安装

注意：以上是快捷安装，也可以常规安装，如下：

```shell
# 安装 `yum-utils` 软件，提供了 `yum-config-manager` 命令来安装 YUM 源
sudo yum install -y yum-utils

# 使用 `yum-config-manager` 命令，添加 docker-ce 的 YUM 源
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 更新 YUM 源的索引
yum makecache fast

# 安装 docker-ce 软件
yum install docker-ce -y

# 查看 docker-ce 版本
docker --version
```

### 3、启动Docker

```shell
sudo systemctl start docker
```

### 4、运行hello-world容器

``` shell
sudo docker run hello-world
```

### 5、设置开机自启动

```shell
sudo systemctl enable docker
```

### 6、配置镜像加速

通过以下配置，可以实现阿里云镜像加速服务：

可以通过修改daemon配置文件/etc/docker/daemon.json来使用加速器

```shell
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://g6eejlq3.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 7、加入docker用户组

如果要使用 作为非 root 用户使用Docker命令，需要将用户添加到 docker用户组：

```bash
sudo usermod -aG docker $USER
```

## Docker工作原理

​		Docker是一个Client-Server结构的系统，Docker守护进程运行在主机上， 然后通过Socket连接从客户端访问，守护进程从客户端接受命令并管理运行在主机上的容器。

Docker启动速度快的原因：

- docker有着比虚拟机更少的抽象层。由于docker不需要Hypervisor实现硬件资源虚拟化，运行在docker容器上的程序直接使用的都是实际物理机的硬件资源。因此在CPU、内存利用率上docker将会在效率上有明显优势。


- docker利用的是宿主机的内核,而不需要Guest OS。因此,当新建一个容器时,docker不需要和虚拟机一样重新加载一个操作系统内核。仍而避免引寻、加载操作系统内核返个比较费时费资源的过程,当新建一个虚拟机时,虚拟机软件需要加载Guest OS,返个新建过程是分钟级别的。而docker由于直接利用宿主机的操作系统,则省略了返个过程，因此新建一个docker容器只需要几秒钟。

# Docker命令

官方文档：https://docs.docker.com/engine/reference/commandline/docker/

## Docker的辅助命令

- `docker version`

  官方文档：https://docs.docker.com/engine/reference/commandline/version/

- `docker info`

  官方文档：https://docs.docker.com/engine/reference/commandline/info/

- `docker --help`

## 镜像命令

### `docker images`

官方文档：https://docs.docker.com/engine/reference/commandline/images/

`docker images`：列出本地的镜像，镜像信息字段如下：

- REPOSITORY：镜像的仓库源
- TAG：镜像的标签
- CREATED：镜像创建时间
- SIZE：镜像大小

> 同一REPOSITORY可以有不同TAG的镜像，使用REPOSITORY:TAG来唯一标识对象，默认的TAG是latest

#### 常用参数

- `-a`：列出本地所有镜像（含中间映像层）

- `-q`：只显示镜像ID

  如：删除全部镜像

  ``` shell
  docker rmi -f $(docker images -aq)
  ```

- `--digests`：显示镜像的摘要信息

- `--no-trunc`：显示完整的镜像信息

显示本地镜像的详细信息：

```shell
docker images -a --digests --no-trunc
```



### `docker search`

官方文档：https://docs.docker.com/engine/reference/commandline/search/

`docker search 某个镜像名称`：到https://hub.docker.com/查询镜像的相关信息 ，信息字段如下：

- NAME：镜像名称
- DESCRIPTION：镜像描述
- STARS：点赞数
- OFFICIAL：是否是官方版
- AUTOMATED：是否是自动构建

#### 常用参数

- `-f, --filter  过滤条件`：根据提供的条件过滤输出
  - `-f STARS=点赞数n`：只显示点赞数大于n的镜像
- `--format`：使用Go模板的更好的打印搜索结果
- `--no-trunc`：显示完整的镜像信息
- `--limit`：最多显示的搜索结果数，默认是25

示例：查询点赞数大于30的nginx，并显示完整信息

```shell
docker search -f STARS=30 --no-trunc nginx
```

### `docker pull`

官方文档：https://docs.docker.com/engine/reference/commandline/pull/

`docker pull 镜像名称[:TAG]`：从远端镜像仓库拉取镜像到本地镜像仓库。

注意：如果没有指定TAG，默认使用的是latest版本的镜像。

示例：拉取nginx的镜像

``` shell
docker pull nginx
```

### `docker rmi`

官方文档：https://docs.docker.com/engine/reference/commandline/rmi/

`docker rmi 镜像1 镜像2 ...`：删除镜像

参数：

- `-f`：强制删除

示例：

删除hello-world镜像：

```shell
docker rmi hello-world:latest
```

删除全部镜像

``` shell
docker rmi -f $(docker images -aq)
```





## 容器

进入容器中，如：进入mysql容器中：

```
docker exec -it mysql /bin/bash
```

- 

# Dockerfile

官方教程：https://docs.docker.com/engine/reference/builder/

- RUN：相当于执行`/bin/sh -c 命令`



# Docker系统镜像

## Busybox

`Busybox` 官网：https://busybox.net/

`Busybox` 官方仓库：https://git.busybox.net/busybox/

`Busybox` 官方镜像：https://hub.docker.com/_/busybox/

`Busybox` 官方仓库：https://github.com/docker-library/busybox



参考：https://yeasy.gitbook.io/docker_practice/os/busybox

## Alpine

`Alpine` 官网：https://www.alpinelinux.org/

`Alpine` 官方仓库：https://github.com/alpinelinux

`Alpine` 官方镜像：https://hub.docker.com/_/alpine/

`Alpine` 官方镜像仓库：https://github.com/gliderlabs/docker-alpine

参考文章：https://yeasy.gitbook.io/docker_practice/os/alpine



### Alipne是什么

​		`Alpine` 操作系统是一个面向安全的轻型 `Linux` 发行版。它不同于通常 `Linux` 发行版，`Alpine` 采用了 `musl libc` 和 `busybox` 以减小系统的体积和运行时资源消耗，但功能上比 `busybox` 又完善的多。

​		在保持瘦身的同时，`Alpine` 还提供了自己的包管理工具 `apk`，可以通过 `https://pkgs.alpinelinux.org/packages` 网站上查询包信息，也可以直接通过 `apk` 命令直接查询和安装各种软件。

​		`Alpine` Docker 镜像也继承了 `Alpine Linux` 发行版的这些优势。相比于其他 `Docker` 镜像，它的容量非常小，仅仅只有 **5 MB** 左右（对比 `Ubuntu` 系列镜像接近 `200 MB`），且拥有非常友好的包管理机制。官方镜像来自 `docker-alpine` 项目。

### 获取并使用Alipne镜像

由于Alipne镜像很小，下载时间往往很短，读者可以直接使用 `docker run` 指令直接运行一个 `Alpine` 容器，并指定运行的 Linux 指令，例如：

```shell
$ docker run alpine echo '123'
123
```

### 迁移至 `Alpine` 基础镜像

目前，大部分 Docker 官方镜像都已经支持 `Alpine` 作为基础镜像，可以很容易进行从其他基础镜像迁移到Alpine基础镜像。

例如：

- `ubuntu/debian` -> `alpine`
- `python:3` -> `python:3-alpine`
- `ruby:2.6` -> `ruby:2.6-alpine`

另外，如果使用 `Alpine` 镜像替换 `Ubuntu` 基础镜像，安装软件包时需要用 `apk` 包管理器替换 `apt` 工具，如

```
$ apk add --no-cache <package>
```

### Alpine软件安装包

​		如果使用 `Alpine` 镜像替换 `Ubuntu` 基础镜像，安装软件包时需要用 `apk` 包管理器替换 `apt` 工具，如

```
$ apk add --no-cache <package>
```

​		`Alpine` 中软件安装包可以在 `https://pkgs.alpinelinux.org/packages` 网站搜索并确定安装包名称。

​		如果需要的安装包不在主索引内，但是在测试或社区索引中。那么可以按照以下方法使用这些安装包。

```shell
$ echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
$ apk --update add --no-cache <package>
```

​		由于在国内访问 `apk` 仓库较缓慢，建议在使用 `apk` 之前先替换仓库地址为国内镜像。

```shell
RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories \
      && apk add --no-cache <package>
```

### apk命令详解

通过使用`apk --help`可以查看apk命令的详细使用说明，这里简单介绍一下，使用命令如下：

```shell
 apk [<OPTIONS>...] COMMAND [<ARGUMENTS>...]
```

- 安装和卸载软件包
  - add：安装
  - del：卸载
- 系统维护
  - fix：修复、重新安装或升级软件包
  - update：更新存储库索引
  - upgrade ：从存储库中安装可用的升级
  - cache：理本地包缓存
- 查询软件包信息
  - info：提供有关包或存储库的详细信息
  - list ：列出与模式或其他条件匹配的包
  - dot：将依赖项呈现为graphviz图
  - policy：显示包的存储库策略
- 存储库维护
  - index：从包创建存储库索引文件
  - fetch：将包从全局存储库下载到本地目录
  - manifest：显示包内容的校验和
  - verify：验证包完整性和签名
- 其他
  - audit：变更审核制度
  - stats：显示有关存储库和安装的统计信息
  - version：比较包版本或对版本字符串执行测试

更多信息：查看`man 8 apk`







# 安装常用服务

## 安装Mysql

1、拉取镜像

```
docker pull mysql:5.7
```

2、启动镜像

```
docker run -p 3306:3306 --name mysql \
-v /workspace/mysql/log:/var/log/mysql \
-v /workspace/mysql/data:/var/lib/mysql \
-v /workspace/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=123456 \
-d mysql:5.7
```

参数说明：

- `-p 3306:3306` 将容器的3306端口映射到主机的3306端口
  - `-v /workspace/mysql/log:/var/log/mysql`将日志文件夹挂载到主机
  - `-v /workspace/mysql/data:/var/lib/mysql`将数据文件夹挂载到主机
  - `-v /workspace/mysql/conf:/etc/mysql` 将配置文件夹挂载到主机
  - `-e MYSQL_ROOT_PASSWORD=123456`初始化Mysql中root用户的密码

3、启动之后，就可以连接了。

4、修改mysql的配置文件

创建配置文件

``` shell
vi /workspace/mysql/conf/my.cnf
```

内容如下：

``` properties
[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
init_connect='set collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```

5、重启mysql

``` shell
docker restart mysql
```

6、自动重启

```shell
docker update mysql --restart=alway
```

## 安装Postgresql

1、拉取镜像

``` shell
docker pull postgres
```

2、启动容器

``` shell
docker run -p 5432:5432 -d \
    --name postgresql \
    -e POSTGRES_PASSWORD=123456 \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /workspace/postgresql:/var/lib/postgresql/data \
    postgres
```

- `-e POSTGRES_PASSWORD=123456`：初始化Postgresql中postgres用户的密码
- `-e PGDATA`：指定pg数据的路径
- `-v /workspace/postgresql:/var/lib/postgresql/data`：映射本地的路径

3、使用psql

```shell
docker exec -it postgres psql -h 127.0.0.1 -U postgres
```



## 安装redis

1、拉取镜像

```shell
docker pull redis
```

2、创建配置文件

```shell
mkdir -p /workspace/redis/conf
touch /workspace/redis/conf/redis.conf
```

3、启动容器

``` shell
docker run --name redis \
-p 6379:6379 \
-v /workspace/redis/data:/data \
-v /workspace/redis/conf/redis.conf:/etc/redis/redis.conf \
-d redis redis-server /etc/redis/redis.conf
```

4、测试redis

``` shell
docker exec -it redis redis-cli
```

进入redis后：

``` shell
127.0.0.1:6379> set test 123456
OK
127.0.0.1:6379> get test
"123456"
127.0.0.1:6379> exit
```

5、修改配置文件，进行内容持久化

``` shell
vi /workspace/redis/conf/redis.conf
```

内容如下：

``` properties
appendonly yes
```

6、重启redis

``` shell
docker restart redis
```

这时，再测试redis，可以看到存储的内容，重启后，依旧存在。

7、自动重启

```shell
docker update redis --restart=alway
```



# Docker-compose

## Docker-compose安装

官方教程：https://docs.docker.com/compose/install/

```shell
# 下载 Docker Compose，并另存为 /usr/local/bin/docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# 给 docker-compose 添加执行权限
chmod +x /usr/local/bin/docker-compose

# 添加 docker-compose 软链到 /usr/bin/ 目录下，方便直接使用
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# 查看 docker-compose 版本
docker-compose --version
```

## Docker-compose命令

官方教程：https://docs.docker.com/compose/reference/



# 问题

## 非root用户使用Docker

通过将用户添加到docker用户组可以将sudo去掉，命令如下

``` shell
sudo groupadd docker #添加docker用户组
sudo gpasswd -a $USER docker #将登陆用户加入到docker用户组中
newgrp docker #更新用户组
```

## window下ubuntu中的docker启动不了

使用 wsl -l 可以列出当前系统上已经安装的 Linux 子系统名称。

```
wsl -l
```

使用 wsl --set-version 2 命令可以设置一个 Linux 发行版的 WSL 版本

``` powershell
wsl --set-version Ubuntu 2
```



