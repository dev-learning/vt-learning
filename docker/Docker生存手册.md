# 走进Docker

## 环境搭建-Centos

### 1、卸载旧的Docker

```shell
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 2、安装Docker

#### 快捷安装

```
curl -sSL https://get.daocloud.io/docker | sh
```

#### 常规安装

注意：以上是快捷安装，也可以常规安装，如下：

```shell
sudo yum install -y yum-utils

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
    
sudo yum install docker-ce docker-ce-cli containerd.io
```

### 3、启动Docker

```shell
sudo systemctl start docker
```

### 4、运行hello-world容器

``` shell
sudo docker run hello-world
```

### 5、设置开机自启动

```shell
sudo systemctl enable docker
```

### 6、配置镜像加速

通过以下配置，可以实现阿里云镜像加速服务：

可以通过修改daemon配置文件/etc/docker/daemon.json来使用加速器

```shell
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://g6eejlq3.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

### 7、加入docker用户组

将当前用户添加到 docker用户组：

```bash
sudo usermod -aG docker $USER
```

## 容器



进入容器中，如：进入mysql容器中：

```
docker exec -it mysql /bin/bash
```



# 安装常用服务

## 安装Mysql

1、拉取镜像

```shell
docker pull mysql:5.7
```

2、启动镜像

```shell
docker run -p 3306:3306 --name mysql-service \
-v /workspace/mysql/log:/var/log/mysql \
-v /workspace/mysql/data:/var/lib/mysql \
-v /workspace/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=mysql52100 \
-d mysql:5.7
```

参数说明：

- `-p 3306:3306` 将容器的3306端口映射到主机的3306端口
  - `-v /workspace/mysql/log:/var/log/mysql`将日志文件夹挂载到主机
  - `-v /workspace/mysql/data:/var/lib/mysql`将数据文件夹挂载到主机
  - `-v /workspace/mysql/conf:/etc/mysql` 将配置文件夹挂载到主机
  - `-e MYSQL_ROOT_PASSWORD=123456`初始化Mysql中root用户的密码

3、启动之后，就可以连接了。

4、修改mysql的配置文件

创建配置文件

``` shell
vi /workspace/mysql/conf/my.cnf
```

内容如下：

``` properties
[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
init_connect='set collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake
skip-name-resolve
```

5、重启mysql

``` shell
docker restart mysql
```

6、自动重启

```shell
docker update mysql --restart=alway
```

5、防火墙配置

``` shell
firewall-cmd --permanent --zone=public --add-port=3306/tcp
firewall-cmd --reload
```



## 安装Postgresql

1、拉取镜像

``` shell
docker pull postgres
```

2、启动容器

``` shell
docker run -p 5432:5432 -d \
    --name postgresql-service \
    -e POSTGRES_PASSWORD=pg52100 \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /workspace/postgresql:/var/lib/postgresql/data \
    postgres
```

- `-e POSTGRES_PASSWORD=123456`：初始化Postgresql中postgres用户的密码
- `-e PGDATA`：指定pg数据的路径
- `-v /workspace/postgresql:/var/lib/postgresql/data`：映射本地的路径

3、使用psql

```shell
docker exec -it postgresql-service psql -h 127.0.0.1 -U postgres
```

4、自动重启

```shell
docker update  postgresql-service --restart=alway
```

5、防火墙配置

``` shell
firewall-cmd --permanent --zone=public --add-port=5432/tcp
firewall-cmd --reload
```



## 安装redis

1、拉取镜像

```shell
docker pull redis
```

2、创建配置文件

```shell
mkdir -p /workspace/redis/conf
touch /workspace/redis/conf/redis.conf
```

3、启动容器

``` shell
docker run --name redis \
-p 6379:6379 \
-v /workspace/redis/data:/data \
-v /workspace/redis/conf/redis.conf:/etc/redis/redis.conf \
-d redis redis-server /etc/redis/redis.conf
```

4、测试redis

``` shell
docker exec -it redis redis-cli
```

进入redis后：

``` shell
127.0.0.1:6379> set test 123456
OK
127.0.0.1:6379> get test
"123456"
127.0.0.1:6379> exit
```

5、修改配置文件，进行内容持久化

``` shell
vi /workspace/redis/conf/redis.conf
```

内容如下：

``` properties
appendonly yes
```

6、重启redis

``` shell
docker restart redis
```

这时，再测试redis，可以看到存储的内容，重启后，依旧存在。

7、自动重启

```shell
docker update redis --restart=alway
```



## 安装nginx

拉取官方的最新版本的镜像：

``` shell
docker pull nginx:latest
```

运行容器

``` shell
docker run \
  --name nginx-service \
  -d -p 80:80 \
  -v /workspace/nginx/html:/usr/share/nginx/html:ro \
  -v /workspace/nginx/config/nginx.conf:/etc/nginx/nginx.conf:ro \
  -v /workspace/nginx/config/conf.d:/etc/nginx/conf.d \
  -v /workspace/nginx/logs:/var/log/nginx \
  nginx
```



nginx.conf

``` nginx
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                                      '$status $body_bytes_sent "$http_referer" '
                                                            '"$http_user_agent" "$http_x_forwarded_for"';
     access_log  /var/log/nginx/access.log  main;

     sendfile        on;
     #tcp_nopush     on;

     keepalive_timeout  65;

     #gzip  on;
	 # 包含conf.d 配置路径
     include /etc/nginx/conf.d/*.conf;
}
```

default.conf配置内容

见到那的

``` nginx
server {
  location /api {
      rewrite /api/(.*) /$1 break;
      proxy_pass  http://192.168.10.140:32274/;
      proxy_set_header X-Real-IP $remote_addr;
  }
}
```

复杂的

``` nginx
  include mime.types;
  default_type application/octet-stream;
  client_header_buffer_size 32k;
  large_client_header_buffers 4 32k;
  client_max_body_size 356m;
  log_format access '$remote_addr - $remote_user [$time_local] "$request" '
				'$status $body_bytes_sent "$http_referer" '
				'"$http_user_agent" "$http_x_forwarded_for"';
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 65;
  server_tokens off;
  client_body_buffer_size 512k;
  proxy_connect_timeout 5;
  proxy_send_timeout 60;
  proxy_read_timeout 5;
  proxy_buffer_size 16k;
  proxy_buffers 4 64k;
  proxy_busy_buffers_size 128k;
  proxy_temp_file_write_size 128k;
  gzip on;
  gzip_min_length 1k;
  gzip_buffers 4 16k;
  gzip_http_version 1.1;
  gzip_comp_level 2;
  gzip_types text/plain application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png;  #压缩文件类型;
  gzip_vary on;
  gzip_disable "msie6"; #不使用gzip IE6;

  upstream apicluster {
    server 49.233.207.213:18080 weight=1 fail_timeout=10s max_fails=1 ;
  }
  server {
    server_name www.ihelpspace.com;
    listen 80;
    charset utf-8;
    error_page 500 502 503 504  /50x.html;
    location / {
      root /usr/share/nginx/html/www;
      index index.html;
      rewrite index.html permanent;
      try_files $uri $uri/ /index.html;
    }
  }
  server {
    server_name linux.ihelpspace.com;
    listen 80;
    location / {
      root /usr/share/nginx/html/linux-command;
      index index.html;
    }
  }
  server {
    server_name jdk.ihelpspace.com;
    listen 80;
    location / {
      root /usr/share/nginx/html/jdk1.8;
      index index.html;
    }
  }
  server {
    server_name desk.ihelpspace.com;
    listen 80;
    charset utf8;
    location / {
      root //usr/share/nginx/html/desk;
      index index.html;
    }
  }
   location /api {
      rewrite /api/(.*) /$1 break;
      proxy_pass  http://apicluster/;
      proxy_set_header X-Real-IP $remote_addr;
 }
```

bak

``` nginx
  client_header_buffer_size 32k;
  large_client_header_buffers 4 32k;
  client_max_body_size 356m;

  tcp_nopush on;
  tcp_nodelay on;
  server_tokens off;
  client_body_buffer_size 512k;
  proxy_connect_timeout 5;
  proxy_send_timeout 60;
  proxy_read_timeout 5;
  proxy_buffer_size 16k;
  proxy_buffers 4 64k;
  proxy_busy_buffers_size 128k;
  proxy_temp_file_write_size 128k;
  gzip on;
  gzip_min_length 1k;
  gzip_buffers 4 16k;
  gzip_http_version 1.1;
  gzip_comp_level 2;
  gzip_types text/plain application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png;  #压缩文件类型;
  gzip_vary on;
  gzip_disable "msie6"; #不使用gzip IE6;

  upstream apicluster {
    server 49.233.207.213:18080 weight=1 fail_timeout=10s max_fails=1 ;
  }
  server {
    server_name www.ihelpspace.com;
    listen 80;
    charset utf-8;
    error_page 500 502 503 504  /50x.html;
    location / {
      root /usr/share/nginx/html/www;
      index index.html;
    }
  }
  server {
    server_name linux.ihelpspace.com;
    listen 80;
    location / {
      root /usr/share/nginx/html/linux-command;
      index index.html;
    }
  }
  server {
    server_name jdk.ihelpspace.com;
    listen 80;
    location / {
      root /usr/share/nginx/html/jdk1.8;
      index index.html;
    }
  }
  server {
    server_name desk.ihelpspace.com;
    listen 80;
    charset utf8;
    location / {
      root /usr/share/nginx/html/desk;
      index index.html;
    }
  }
  server {
    server_name api.ihelpspace.com;
    listen 80;
    location / {
      rewrite /(.*) /$1 break;
      proxy_pass  http://apicluster/;
      proxy_set_header X-Real-IP $remote_addr;
   }
  }
```





防火墙配置

``` shell
firewall-cmd --permanent --zone=public --add-port=80/tcp
firewall-cmd --reload
```



# 问题

## 非root用户使用Docker

通过将用户添加到docker用户组可以将sudo去掉，命令如下

``` shell
sudo groupadd docker #添加docker用户组
sudo gpasswd -a $USER docker #将登陆用户加入到docker用户组中
newgrp docker #更新用户组
```

## Error response

Error response from daemon: driver failed programming external connectivity on endpoint mysql-service (0c58dc659270c49077b487a385aebe714c06e18ede4bf6fec23c0ee64d389538):  (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 3306 -j DNAT --to-destination 172.17.0.3:3306 ! -i docker0: iptables: No chain/target/match by that name.
 (exit status 1))

**解决方案：重启docker**

```
systemctl restart docker
```

