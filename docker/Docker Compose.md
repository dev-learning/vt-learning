## 参考书籍

- <<Docker实战>> , 源码地址:https://github.com/dockerinaction

## 认识Docker Compose

Docker Compose（也称Compose）是一个用于定义、启动和管理服务的工具，服务可以定义为Docker容器的一个或多个副本。

再YAML文件中定义了服务和服务系统，并通过命令行docker-compose进行管理，通过Compose，可以方便的进行如下任务：

- 构建Docker镜像
- 启动容器化的应用和服务
- 穷的那个完整的服务系统
- 管理系统中单个服务的状态
- 服务伸缩
- 产看生成服务的容器的收集日志

Compose不在专注于单个容器，而是描述完整的环境以及服务组件的交互。一个Compose文件可能描述多个单独的服务，他们是相互关联的，但应保持隔离和独立伸缩。

## 安装

下载：

``` shell
wget https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64
```

```shell
mv docker-compose-Linux-x86_64 /usr/local/bin/docker-compose
```

将可执行权限应用于二进制文件：

``` shell
sudo chmod +x /usr/local/bin/docker-compose
```

创建软链：

``` shell
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

测试是否安装成功：

``` shell
docker-compose --version
```



## Docker-compose命令

启动

进入含有docker-compose.yml文件的目录下，使用以下命令启动：

```
docker-compose up -d
```

查看服务：

```
docker-compose ps
```

关闭环境：

```
docker-compose stop
docker-compose kill
```

停止服务：

```
docker-compose down
```

删除服务的容器：

```
docker-compose rm -v
# 强制删除
docker-compose rm -vf
```

查看日志:

```
docker-compose logs
docker-compose logs 服务名 服务名
```

重新构建一个或所有服务:

```
docker-compose build
docker-compose build 服务名
```

服务伸缩:

``` 
docker-compose scale 服务名=服务数
```





## docker-compose.yml

### wordpress示例

``` yaml
# Filename:docker-compose.yml
# 定义命名为wordpress的服务
wordpress:
  # 使用官方wordpress4.2.2镜像
  images: wordpress:4.2.2
  links: # 链接依赖到db服务
    - db: mysql
  ports: # 映射容器的80到宿主机端口8080
    - 8080: 80

# 定义命名为db的服务
db:
  # 使用官方mariadb:latest镜像
  image: mariadb
  environment: # 通过环境变量设置数据库系统管理员密码
    MYSQL_ROOT_PASSWORD: 123456
```

通过`docker-compose up -d`可以启动服务，然后通过：http://localhost:8080 可以访问到wordpress服务 

### Docker Registry示例

源码地址:https://github.com/dockerinaction/ch11_notifications

docker-compose.yml:

```yaml
# A Docker Distribution based registry. The service listens on port 5000.
# This registry has been specialized to push notifications to
# "webhookmonitor:3000" which in this environment is filled by the "pump"service.
registry:
  build: ./registry
  ports:
    - "5555:5000"
  links:
    - pump:webhookmonitor

# This is a small NodeJS application that listens on port 3000 and pumps
# received JSON messages to an Elasticsearch node. The application itself keeps no state.
pump:
  build: ./pump
  expose:
    - "8000"
  links:
    - elasticsearch:esnode
  
# The elasticsearch image declares a volume at /usr/share/elasticsearch/data
# for that reason we need not declare a volume here unless we want to bind-mount that volume to a specific location on the disk.
# Doing so may be useful for integration with volume management tools
elasticsearch:
  image: elasticsearch:1.5
  ports:
    - "9200:9200"
  command: "-Des.http.cors.enabled=true"

# Calaca is stateless and client side only.
# NPM is used for a simple web server.
calaca:
  build: ./calaca
  ports:
    - "3000:3000"
```

通过`docker-compose up -d`可以启动服务，然后通过`docker-compose logs`可以自动追踪服务日志

注意:

> `docker-compose up -d`会停止当前并删除运行的容器,接着创建新的容器,并重新赋着前代环境挂载的书籍卷.

### Coffee-api示例

源码地址：https://github.com/dockerinaction/ch11_coffee_api

```yaml
data:
  image: gliderlabs/alpine
  command: echo Data Container
  user: 999:999
  labels:
    com.dockerinaction.chapter: "11"
    com.dockerinaction.example: "Coffee API"
    com.dockerinaction.role: "Volume Container"

dbstate:
  extends:
    file: docker-compose.yml
    service: data
  volumes:
    - /var/lib/postgresql/data/pgdata

# the postgres image uses gosu to change to the postgres user
db:
  image: postgres
  volumes_from:
    - dbstate
  environment:
    - PGDATA=/var/lib/postgresql/data/pgdata
    - POSTGRES_PASSWORD=development
  labels:
    com.dockerinaction.chapter: "11"
    com.dockerinaction.example: "Coffee API"
    com.dockerinaction.role: "Database"

# the nginx image uses user de-escalation to change to the nginx user
proxy:
  image: nginx
  restart: always
  volumes:
    - ./proxy/app.conf:/etc/nginx/conf.d/app.conf
  ports:
    - "8080:8080"
  links:
    - coffee
  labels:
    com.dockerinaction.chapter: "11"
    com.dockerinaction.example: "Coffee API"
    com.dockerinaction.role: "Load Balancer"

coffee:
  build: ./coffee
  user: 777:777
  restart: always
  expose:
    - 3000
  ports:
    - "0:3000"
  links:
    - db:db
  environment:
    - COFFEEFINDER_DB_URI=postgresql://postgres:development@db:5432/postgres
    - COFFEEFINDER_CONFIG=development
    - SERVICE_NAME=coffee
  labels:
    com.dockerinaction.chapter: "11"
    com.dockerinaction.example: "Coffee API"
    com.dockerinaction.role: "Application Logic"

```

编译coffee服务（以上4个组件，只有coffee需要编译）：

```
docker-compose build
```

拉去本地不存在的镜像：

```
docker-compose pull
```

启动服务：

```
docker-compose up -d
# 不需要依赖关系重启
docker-compose up --no-dep -d proxy
```

在浏览器访问呢：http://localhost:8080/api/coffeeshops/

